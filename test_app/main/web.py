from flask import jsonify, abort, request, Blueprint
from urllib.parse import urlparse

import uuid
import os
import requests


from test_app import db
from test_app.models.item import Item


web = Blueprint('frontend', __name__,
                template_folder='templates')


@web.route("/", methods=["POST"])
def verify_docker_setup():
    """Dead simple route to verify docker-compose setup is working fine"""

    # Verify we can make outbound network requests
    r = requests.get('https://www.statuspage.io')
    if r.status_code != 200:
        return 'Network connectivity check to www.statuspage.io failed', 500
    
    # Verify we can save something to the db
    item_to_save = Item('sample')
    db.session.add(item_to_save)
    db.session.commit()

    # Verify we can read something from the DB
    read_item = Item.query.order_by('-id').first()
    if read_item is None:
        return 'Reading from database failed - missing expected item', 500
    if read_item.name != 'sample':
        return 'Reading from database failed - unexpected item name {}'.format(read_item.name), 500

    # if you can't get to this point - don't sweat it too much; you can use one of our laptops.
    return 'Docker and docker-compose are set up correctly - good work!\n'
