from sqlalchemy.dialects import postgresql
from bs4 import BeautifulSoup
import requests

from test_app import db

class Item(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    
    def __init__(self, name):
        self.name = name
