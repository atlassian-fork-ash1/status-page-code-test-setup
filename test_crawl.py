from test_app import gen_app, db, TestConfig, celery

import pytest

@pytest.fixture(scope='session')
def new_app(request):
    app = gen_app(TestConfig())
    celery.conf.update(task_always_eager=True)
    ctx = app.app_context()
    ctx.push()

    def release_context():
        ctx.pop()

    request.addfinalizer(release_context)
    return app

@pytest.fixture(scope='session')
def client(new_app):
    return new_app.test_client()

@pytest.fixture(autouse=True)
def setup_db(new_app):
    db.create_all()
    db.session.begin_nested()
    yield
    db.session.rollback()

def test_make_sure_tests_work():
    r = 'my result'
    assert r == 'my result'
