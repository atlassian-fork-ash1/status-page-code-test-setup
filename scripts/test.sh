#!/usr/bin/env sh

docker-compose up -d postgres redis
docker-compose run --rm web pytest test_crawl.py
