from test_app import gen_app, db

app = gen_app()

@app.cli.command()
def seed():
    db.drop_all()
    db.create_all()

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)
